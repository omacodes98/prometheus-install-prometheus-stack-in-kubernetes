# Install Prometheus Stack in Kubernetes 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

Install Prometheus Stack in Kubernetes 

## Technologies Used 

* Prometheus 

* Kubernetes 

* Helm 

* AWS EKS 

* eksctl 

* Grafana 

* Linux 

## Steps 

Step 1: Create a cluster with eksctl 

    eksctl create cluster 

[Creating cluster](/images/01%20use%20eksctl%20to%20create%20a%20cluster.png)
[Cluster nodes](/images/02%20cluster%20nodes%20created%20on%20aws.png)

Step 2: Deploy online shop microservices application in the cluster node using a manifest file, in my case from a previous project i worked on which is the microservices project

    kubectl apply -f config.yaml 

[Deploying Microservices application](/images/03%20deploy%20online%20shop%20microservices%20application%20in%20the%20cluster%20note%20this%20is%20a%20deployment%20file%20in%20which%20i%20created%20for%20my%20microservices%20project.png)
[Pods running](/images/04%20pods%20running.png)

Step 3: add helm repo where the chat is located

    helm repo add prometheus-community https://github.com/prometheus-community/helm-charts 

    helm repo update 

[Adding helm repo](/images/05%20add%20helm%20repo%20where%20the%20chat%20is%20located.png)

Step 4: create own dedicated namespace for prometheus monitoring

    kubectl create namespace monitoring 

[Namespace created](/images/06%20creare%20its%20own%20dedicated%20namespace%20monitoring.png)

Step 5: Install helm chart in the created namespace - monitoring

    helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring 

[Installing helm chart](/images/07%20install%20helm%20chart%20in%20the%20created%20namespace%20-%20mmonitoring%20.png)

Step 6: Check components that is running in cluster 

    kubectl get all -n monitoring

[All components running in cluster](/images/09%20components%20that%20were%20deployed%20in%20montioring%20namespace.png) 


## Installation

    brew install eksctl 

## Usage 

    kubectl apply -f config.yaml

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/prometheus-install-prometheus-stack-in-kubernetes.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/prometheus-install-prometheus-stack-in-kubernetes

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.